#!/usr/bin/env python3
""" CS 8803: AI, Ethics, and Society
Word Analogy Exercise Tool

This tool automates the otherwise tedious clicking and typing necessary to
complete the word analogy assignment for the AI, Ethics, and Society class
as taught at the Georgia Institute of Technology. It is free to use and
distribute, subject to the BSD 3-Clause License as found in the LICENSE file.

All credit for the online tool goes to spinningbytes.com.

Copyright 2020 Christopher Pitts
"""

from argparse import ArgumentParser
from itertools import combinations
from pathlib import Path

import requests

url = "http://4530.hostserv.eu/api/embedding/doesntMatch/en"

# Assignment requires exactly ten words
n_words = 10

# Tool supports two genders, exactly "man" or "woman"
genders = ["man", "woman"]


def existing_path(path):
    """ Validate that path exists on disk

    Parameters
    ----------
    path : str, Path
      Any object that can be converted into a Path object

    Returns
    -------
    path : Path
      Resolved path on disk

    Raises
    ------
    ValueError
      When the supplied path does not exist on disk
    """
    if not isinstance(path, Path):
        path = Path(path)

    if not path.resolve().exists():
        raise ValueError("{path} not found")

    return path.resolve()


def parse_args():
    """ Parse and validate arguments

    Parameters
    ----------
    None

    Returns
    -------
    argparse.Namespace
      Parsed and validated arguments
    """
    argp = ArgumentParser()
    argp.add_argument("path", help="path to input file", type=existing_path)
    argp.add_argument(
        "--output",
        required=False,
        default="results.csv",
        help="path to output file, defaults to 'results.csv'",
        type=Path,
    )

    return argp.parse_args()


def main(args):
    """ Main execution

    Parameters
    ----------
    args : argparse.Namespace
      Parsed arguments

    Returns
    -------
    None
    """
    with open(args.path, "r") as in_file:
        words = in_file.read().strip().split("\n")

    if len(words) != n_words:
        print(
            f"Wrong number of words, must supply exactly {n_words} words (you supplied {len(words)})"
        )

    results = []

    # Execute all two-pair combinations with each gender
    for gender in genders:
        for pair in combinations(words, 2):
            r = requests.get(f"{url}/{gender}/{pair[0]}/{pair[1]}")
            result = f"{gender} {pair[0]} {pair[1]} -> {r.text}"
            print(result)
            results.append(f"{gender},{pair[0]},{pair[1]},{r.text}")

    # Dump to output CSV file
    with open(args.output, "w") as out_file:
        out_file.write("gender,word0,word1,removed_word\n")
        for result in results:
            out_file.write(f"{result}\n")


if __name__ == "__main__":
    args = parse_args()
    main(args)
