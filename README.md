# Word Analogy

A script to automate the data entry for the word analogy assignment.

## Usage
The basic usage is very simple. Given a word file with one word per line:

```
python3 run.py <path/to/words/file>
```

If a file `words.txt` is located at `/home/gpburdell/words.txt`:

```
python3 run.py /home/gpburdell/words.txt
```

The full help string is:

```
usage: run.py [-h] [--output OUTPUT] path

positional arguments:
  path             path to input file

optional arguments:
  -h, --help       show this help message and exit
  --output OUTPUT  path to output file, defaults to 'results.csv'
```

## Dependencies
The only dependency is the Python `requests` library. If you need to create a Python virtual environment to install that package, [here is a get-started guide](https://wiki.cwpitts.com/languages/python/virtual_environments/) on the two main solutions for environment management, [Anaconda](https://www.anaconda.com/products/individual) and [Pipenv](https://pipenv.pypa.io/en/latest/).
